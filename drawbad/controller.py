import time
import json
import copy
from channels.layers import get_channel_layer

from drawbad.loop import GameObject
from drawbad.signals import sync_request
from drawbad import common

logger = common.getLogger('drawbad.controller', "DEBUG")

class GameController(GameObject):
  def __init__(self, game_id, loop):
    self.game_id = game_id
    self.state = {'stage': 'lobby'}
    super(GameController, self).__init__(loop=loop)

  def revive(self, state):
    self.state = state

  async def on_sync(self):
    sync_request.send(sender=self, game_id=self.game_id)

  def die(self):
    self.trigger('die', 0)

  async def update_player(self, id, data):
    layer = get_channel_layer()

    msg = {
      "players": [{ "id": id, "state": data }]
    }
    await layer.group_send(f"game_{self.game_id}", 
      {'type': 'update_state', 'objects': msg})

  async def update_game(self, data):
    layer = get_channel_layer()
    msg = {
      "id": id, "state": data
    }
    await layer.group_send(f"game_{self.game_id}",
      {'type': 'update_state', 'game': msg})

  async def join(self, user):
    user_id = user.id
    username = user.username
    is_new = False
    if 'players' not in self.state:
      self.state['players'] = {}
    if str(user_id) not in self.state['players']:
      self.state['players'][str(user_id)] = {'username': username}
      is_new = True
    self.state['players'][str(user_id)].update({'joined': True, 'new_key': 'joined'})
    self.state['player_count'] = len(self.state['players'])
    logger.info("Player {} joined game {} - new {}".format(username, self.game_id, is_new))
    await self.update_player(user.id, {'joined': True, 'username': user.username})
    #self.trigger('sync', 0)

  async def drop(self, user):
    user_id = str(user.id)
    logger.info("Player {} dropped".format(user.id))
    if self.state['players'] and self.state['players'][user_id]:
      self.state['players'][user_id]['joined'] = False
      await self.update_player(user.id, {'joined': False})

  async def ready(self, user):
    if self.state.get('stage') == 'lobby':
      self.state['players'][str(user.id)]['ready'] = True
      logger.info("Player {} readied".format(user.username))
      await self.update_player(user.id, {'ready': True})
      return True
    return False

  async def game_init(self):
    self.state['stage'] = 'choose_words'
    await self.update_game() 

  async def start(self, user):
    if self.state.get('stage') == 'lobby':
      players = self.state.get('players')
      if len(players) > 0 and all([x.get('ready', False) for x in players.values()]):
        self.state['stage'] = "game_init"
        self.trigger('sync', 0)
      else:
        raise Exception("Not all players ready")
    else:
      raise Exception("Must start from lobby")

  def listen(self, user):
    pass

  def persist(self):
    p_state = copy.deepcopy(self.state)
    for pid, player in p_state.get('players', {}).items():
      player.pop('joined', None)
    return p_state
