# Generated by Django 3.0.4 on 2020-03-21 23:51

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('drawbad', '0002_game_over'),
    ]

    operations = [
        migrations.AddField(
            model_name='game',
            name='state',
            field=jsonfield.fields.JSONField(default=dict),
        ),
    ]
