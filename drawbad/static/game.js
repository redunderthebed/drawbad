console.log("oh hello");

var game = {
  stages: [], 
  intervals: [],
  state: {},
  create_canvas: function(){
    $("#paint div").append('<canvas id="myCanvas"></canvas>');

    var canvas = document.getElementById('myCanvas');
    var ctx = canvas.getContext('2d');
     
    var painting = document.getElementById('paint');
    var paint_style = getComputedStyle(painting);
    canvas.width = parseInt(paint_style.getPropertyValue('width'));
    canvas.height = parseInt(paint_style.getPropertyValue('height'));

    var mouse = {x: 0, y: 0};
     
    canvas.addEventListener('mousemove', function(e) {
      mouse.x = e.pageX - this.offsetLeft;
      mouse.y = e.pageY - this.offsetTop;
    }, false);

    ctx.lineWidth = 3;
    ctx.lineJoin = 'round';
    ctx.lineCap = 'round';
    ctx.strokeStyle = '#00CC99';
     
    canvas.addEventListener('mousedown', function(e) {
        ctx.beginPath();
        ctx.moveTo(mouse.x, mouse.y);
     
        canvas.addEventListener('mousemove', onPaint, false);
    }, false);
     
    canvas.addEventListener('mouseup', function() {
        canvas.removeEventListener('mousemove', onPaint, false);
    }, false);

    canvas.addEventListener("touchstart", function (e) {
      mousePos = getTouchPos(canvas, e);
      var touch = e.touches[0];
      var mouseEvent = new MouseEvent("mousedown", {
        clientX: touch.clientX,
        clientY: touch.clientY
      });
      canvas.dispatchEvent(mouseEvent);
      e.preventDefault()
    }, false);
    canvas.addEventListener("touchend", function (e) {
      var mouseEvent = new MouseEvent("mouseup", {});
      canvas.dispatchEvent(mouseEvent);
      e.preventDefault()
    }, false);
    canvas.addEventListener("touchmove", function (e) {
      var touch = e.touches[0];
      var mouseEvent = new MouseEvent("mousemove", {
        clientX: touch.clientX,
        clientY: touch.clientY
      });
      canvas.dispatchEvent(mouseEvent);
      e.preventDefault()
    }, false);
     
    function getTouchPos(canvasDom, touchEvent) {
      var rect = canvasDom.getBoundingClientRect();
      return {
        x: touchEvent.touches[0].clientX - rect.left,
        y: touchEvent.touches[0].clientY - rect.top
      };
    }

    var onPaint = function() {
        ctx.lineTo(mouse.x, mouse.y);
        ctx.stroke();
    };

  },
  'init_stage': function(idx) {
    this.stages[idx]['init']();
    this.start_interval(this.stages[idx]['poll'], 1000);
  },
  'start_interval': function(f, to) {
    var code = setInterval(f, to)
    this.intervals.push(code)
  }
}

game.stages.push({
  'init': function(){
    $('.row.header').empty();
    $('.row.header').append("<div>Connecting...</div>");
  },
  'poll': $.proxy(function(){
    var _this = this;
    $.get('/drawbad/game/' + this.id + '/lobby')
      .done(function(){
        if (_this.state.ready != true){
          $('.row.header').empty();
          $('.row.header').append("<div>You're connected, hit ready when you're ready</div>");
          $('.row.content div').append("<button>Ready</button>");
          _this.state.ready = true;
        }
      });
  }, game)
})
