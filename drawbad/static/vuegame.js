var app = new Vue({
  el: '#drawapp',
  delimiters: ['!{','}'],
  data: {
    game_id: null,
    prompts: {
      'before_join': "Joining..",
      'before_ready': "Hi #USER#, welcome to the game! Hit ready!",
      'after_ready': "Ok #USER#, just waiting for the others",
      'before_start': "Everyone's in, hit start!",
      'after_start': "Nice!"
    },
    prompt: 'before_join',
    state: {},
    my_state: {},
    user: "steve",
    user_id: -1
  },
  methods:{
    set_prompt: function(prompt){
      console.log('set prompt', prompt, app.prompts[prompt]);
      app.prompt = prompt
      //app.prompt = app.prompts[prompt].replace("#USER#", app.user);
    },
    listen: function(){
      console.log('polling')
      $.get('/drawbad/game/' + app.game_id + '/action/', {'action': 'listen'})
        .then(function(response){
          console.log(response)
        })
    },
    poll_state: function(){
      return $.get('/drawbad/gameview/', {'action': 'poll_state', 'game_id': app.game_id})
        .then(function(response){
          app.state = response.data.state
        })
    },
    init_game_state: function(){
      return this.poll_state().then(function(response){
        if (app.state.stage == "lobby") {
          if (app.my_state.joined) {
            if (app.my_state.ready) {
              app.set_prompt('after_ready')
            }
            else{
              app.set_prompt('before_ready')
            }
          }
          else {
            app.set_prompt('before_join')
          }
        }
        app.join()
      });
    },
    peek_state: function(){
      $.get('/drawbad/gameview/', {'action': 'poll_state', 'game_id': app.game_id})
        .then(function(response){
          console.log(response.data.state)
        })
    },
    'socket_receive': function(event){
      event_data = JSON.parse(event.data)
      console.log('socket receive', event_data);
      console.log(event_data.type)
      if (event_data.type == 'update_state') {
        objects = event_data.objects
        if (objects) {
          Object.keys(objects).forEach(function(obj_type){
            hdlr = 'update_' + obj_type
            hdlr = app[hdlr]
            objects[obj_type].forEach(function(obj){
              hdlr(obj.id, obj.state);
            });
          });
        }
        game = event_data.game
        if (game && game.id == this.game_id) {
          self.update_game_state(game.state)
        }
      }
    },
    'update_game_state': function(state) {
      if (state.stage != this.state.stage) {
        trigger = this['on_' + state.stage]
        state.stage = this.state.stage
        if (trigger) {
          trigger()
        }
      }
    },
    'update_players': function(player_id, data) {
      console.log("updating player", player_id, "with", data);
      if (!this.state.players){
        this.init_game_state().then(function(){
          app.update_players(player_id, data)
        });
        return;
      }
      if (!(player_id in this.state.players)) {
        this.state.players[player_id] = {}
        this.state.player_count = Object.keys(this.state.players).length
      }
      player = this.state.players[player_id]
      
      triggers = {'joined': 'on_join', 'ready': 'on_ready'}
      Object.keys(data).forEach($.proxy(function(key){
        if (key == "new_key"){
          return
        }
        if (!(key in player)){
          player['new_key'] = key
        }
        trigger = null;

        if (player[key] != data[key]){
          trigger = triggers[key]
        }
        console.log(key, player[key], data[key], trigger)
        player[key] = data[key]
        if (trigger){
          this[trigger](player_id)
        }
      }, this))
    },
    on_game_init: function() {
      console.log("Game starting");
    },
    join: function(){
      try{
        app.ws = new WebSocket("ws://localhost:8000/ws/game/" + app.game_id + "/");
      }
      catch(err) {
        console.log("Connection Failed:", err);
        app.prompt = "Uh-oh! The connection failed. I guess they hate you and don't want you to join? Or maybe that's just me..."
      }
      app.ws.onerror =function(event) {
        console.log("Connection Error", event);
        app.prompt = "Oh shoot! Connection problem! Guess you're done! GO HOME!"
      }
      app.ws.onopen = function(event) {
        app.ws.send('{"action": "join"}')
      }
      app.ws.onmessage = app.socket_receive

    },
    ready: function(){
      app.ws.send(JSON.stringify({
        "action": "ready"
      }))
    },
    start: function(){
      app.ws.send(JSON.stringify({
        "action": "start"
      }))
    },
    on_ready: function(user_id){
      console.log("player", user_id, "ready")
      if (user_id == this.user_id) {
        if (this.state.stage == "lobby" && this.prompt == "before_ready") {
          this.prompt = "after_ready"
        }
      }
      if (this.state.stage == "lobby"){
        all_ready = Object.values(this.state.players).every(function(player){
          console.log("checking ready", player.username)
          return player.ready
        });
        if (all_ready) {
          this.prompt = "before_start";
        }
      }
      else{
        this.prompt = "game is started"
      }
    },
    on_join: function(user_id){
      console.log("player", user_id, "joined")
      if (user_id == this.user_id) {
        if (this.state.stage == "lobby" && this.prompt == "before_join") {
          this.prompt = "before_ready"
        }
        if (this.my_state.ready) {
          this.on_ready(this.user_id);
        }
      }
    }
  },
  watch: {
    state: {
      'handler': function(newVal, oldVal) {
          console.log(JSON.stringify(newVal), (newVal.players || {})[this.user_id])
          if (newVal.players) {
            new_state = newVal.players[this.user_id] || {}
            this.my_state = new_state
          }
          else {
            this.my_state = {'poop':'scoop'}
          }

        },
      'deep': true
    }
  }
});
