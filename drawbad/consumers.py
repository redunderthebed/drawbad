import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer
from drawbad.views import ensure_thread
from drawbad import common

logger = common.getLogger('drawbad.consumer')
class GameConsumer(AsyncWebsocketConsumer):
  async def connect(self):
    print("CONNECTING", self.channel_name)
    self.game_id = self.scope['url_route']['kwargs']['game_id']
    self.group_name = f"game_{self.game_id}"
    await self.channel_layer.group_add(
      self.group_name,
      self.channel_name
    )
    await self.accept()
  
  async def disconnect(self, close_code):
    thread = ensure_thread(self.game_id)
    await thread.main_go.drop(self.scope['user'])
    await self.channel_layer.group_discard(
      self.group_name, self.channel_name)

  async def update_state(self, event):
    await self.send(text_data=json.dumps(event))

  async def receive(self, text_data):
    data = json.loads(text_data)
    result = None
    errors = {}
    user = self.scope['user']
    thread = ensure_thread(self.game_id)
    ctrler = thread.main_go
    method = data.pop('action', None)
    args = data.pop('args', [])
    kwargs = data
    if method is not None:
      try:
        result = await getattr(ctrler, method)(*args, user=user, **kwargs)
      except TypeError as e:
        logger.error("Method {} should be string not {}".format(method, type(method)))
      except Exception as e:
        logger.error("Exception occured running method {}: {}".format(method, str(e)))
      
    else:
      logger.info("data received from {}: {}".format(user, data))