import django.conf as settings
import logging
import sys
import os
from contextlib import contextmanager
logging.DEBUG1 = 9 
# from logging.config import dictConfig
# dictConfig(settings.logging_config)

logging.addLevelName(logging.DEBUG1, "DEBUG1")
def debug1(self, message, *args, **kws):
    if self.isEnabledFor(logging.DEBUG1):
        # Yes, logger takes its '*args' as 'args'.
        self._log(logging.DEBUG1, message, args, **kws) 
logging.Logger.debug1 = debug1

def getLogger(name, level=None):
  logger = logging.getLogger(name)
  if level in logging._nameToLevel:
    level = logging._nameToLevel[level]
  if level is not None:
    logger.setLevel(level)
  return logger

def is_verbose_test():
  return "-v" in sys.argv

@contextmanager
def override_logger(logger, level):
  old_level = logger.level
  if not settings.LOUD_TESTS:
    logger.setLevel(level)
  try:
  	yield
  finally:
  	logger.setLevel(old_level)