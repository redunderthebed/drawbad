import json

from django.test import TestCase, RequestFactory
from django.contrib.auth import models as authmodels
from drawbad.views import GameActionView

class GameTest(TestCase):
  fixtures = ['users.json']
  def setUp(self):
    self.passwords = {
        "Player1": "AnkleBone", 
        "Player2": "AnkleBone2"
      }
    self.factory = RequestFactory()
    super(GameTest, self).setUp()

  def user_creds(self, username):
    return {'username': username, 
      'password': self.passwords.get(username)}

  def get_data(self, response):
    r_json = response.json()
    self.assertTrue(r_json.get('success'))
    return r_json.get('data')

  def test_game_create(self):
    response = self.client.post('/accounts/login/', self.user_creds('Player1'), follow=False)
    response = self.client.post('/drawbad/gameview/', {'action': 'create_game'})
    r_json = response.json()
    r_data = r_json.get('data', {})
    self.assertTrue(r_json.get('success'))
    self.assertGreater(r_data.get('game_id'), 0)

  def do_game_action(self, user, game_id, action, expect_success=True, **kwargs):
    json_data = {'action': action}
    json_data.update(**kwargs)
    request = self.factory.post('/drawbad/game/{}/action/'.format(game_id), json_data, format='json')
    if isinstance(user, str):
      user = authmodels.User.objects.get(username=user)
    request.user = user
    response = GameActionView.as_view()(request, game_id)
    r_json = json.loads(response.getvalue())
    if expect_success is True:
      self.assertTrue(r_json.get('success'), "Not successful: {}".format(r_json))
      r_data = r_json.get('data')
      return r_data
    else:
      return r_json

  def test_game_lobby(self):
    response = self.client.post('/accounts/login/', self.user_creds('Player1'), follow=False)
    response = self.client.post('/drawbad/gameview/', {'action': 'create_game'})
    r_json = response.json()
    game_id = r_json.get('data', {}).get('game_id')
    
    self.assertTrue(r_json.get('success'))
    self.assertGreater(game_id, 0)
    r_data = self.get_data(self.client.get('/drawbad/gameview/', {'action': 'poll_state', 'game_id': game_id}))
    self.assertEqual(r_data.get('state', {}).get('stage'), 'lobby')
    r_data = self.do_game_action("Player2", 1, "join")
    self.assertTrue(r_data.get('joined'))
    r_data = self.do_game_action("Player1", 1, "join")
    self.assertTrue(r_data.get('joined'))
    state = self.get_data(self.client.get('/drawbad/gameview/', {'action': 'poll_state', 'game_id': game_id})).get('state')    
    self.assertEqual(set(state.get('players').keys()), set(('3', '4')))
    r_data = self.do_game_action("Player1", 1, "ready")
    r_data = self.do_game_action("Player1", 1, "start", expect_success=False)
    self.assertFalse(r_data.get('success'))
    r_data = self.do_game_action("Player2", 1, "ready")
    r_data = self.do_game_action("Player1", 1, "start")
    r_data = self.get_data(self.client.get('/drawbad/gameview/', {'action': 'poll_state', 'game_id': game_id}))
    self.assertEqual(r_data.get('state', {}).get('stage'), 'game')