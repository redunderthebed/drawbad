from django.dispatch import Signal
sync_request = Signal(providing_args=['sender', 'game_id'])