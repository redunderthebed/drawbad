import json
import threading
import time
import queue

from django.shortcuts import render
from django.dispatch import receiver
from django.http import HttpResponse, JsonResponse
from django.views import View
from django.contrib.auth import models as authmodels
from django.contrib.auth.decorators import login_required

from drawbad import models
from drawbad import common
from drawbad.signals import sync_request
from drawbad.loop import Loop, GameObject
from drawbad.controller import GameController
from django.shortcuts import redirect

threads = {}

logger = common.getLogger('drawbad')

def get_game_thread(game_id):
  global threads
  return threads.get(int(game_id))

def store_game_thread(game_id, thread):
  global threads
  threads[int(game_id)] = thread

class SyncerThread(threading.Thread):
  def __init__(self):
    self.queue = queue.SimpleQueue()
    super(SyncerThread, self).__init__(daemon=True)

  def run(self):
    while True:
      while self.queue.empty():
        time.sleep(1)
      game_id = self.queue.get()
      logger.debug("Request to sync {}".format(game_id))
      game = models.Game.objects.get(pk=game_id)
      thread = get_game_thread(game_id)
      controller = thread.main_go
      new_state = controller.persist()
      for k, v in new_state.get('players', {}).items():
        if game.state.get('players', {}).get(k) is None:
          logger.info("Syncing new player {}".format(k))
          game.players.add(k)

      game.state = controller.persist()
      game.save()

  def add_job(self, game_id):
    logger.debug('putting job {}'.format(game_id))
    self.queue.put(game_id)

SYNCER_THREAD = SyncerThread()
SYNCER_THREAD.start()

class GameThread(threading.Thread):
  def __init__(self, game_id, revive_state={}):
    self.loop = None
    self.game_id = game_id
    self.main_go = None
    self.revive_state = revive_state
    logger.info("Game thread revived with state: {}".format(revive_state))
    super(GameThread, self).__init__(daemon=True)

  def run(self):
    self.loop = Loop(new_loop=True) # BAD MAN!!!!
    self.main_go = GameController(self.game_id, loop=self.loop)
    
    if self.revive_state:
      self.main_go.revive(self.revive_state)
      self.revive_state = None

    self.loop.register_object(self.main_go)
    self.loop.start()

  def stop(self):
    self.main_go.trigger('die')

  def is_looping(self):
    return not self.loop.stopped if self.loop is not None else False 

@login_required
def index(request):
  context = {
    'msg': 'hi',
    'games': models.Game.objects.filter(players=request.user).all()
  }
  return render(request, 'drawbad/index.html', context)

@receiver(sync_request)
def sync_handler(*args, **kwargs):
  SYNCER_THREAD.add_job(kwargs.get('game_id'))

class HandlerView(View):
  default_handler = None
  action=None

  def _do_handler(self, request, *args, **kwargs):
    action = self.action or request.GET.get('action', None) or request.POST.get('action', None)
    hdlr = getattr(self, 'action_{}'.format(action), None)
    if hdlr is None:
      if self.default_handler is None:
        return HttpResponse("No handler for {}".format(action))
      else:
        hdlr = getattr(self, self.default_handler)
    return hdlr(request, *args, **kwargs)

  def get(self, request, *args, **kwargs):
    return self._do_handler(request, *args, **kwargs)

  def post(self, request, *args, **kwargs):
    return self._do_handler(request, *args, **kwargs)

  def delete(self, request, *args, **kwargs):
    return self._do_handler(request, *args, **kwargs)

class GameActionView(HandlerView):
  default_handler = 'controller'

  def controller(self, request, game_id, *args, **kwargs):

    method = request.POST.get('action') or request.GET.get('action')
    #import pdb; pdb.set_trace()
    print(method, request.POST, request.GET)
    thread = get_game_thread(game_id)
    ctrler = thread.main_go
    data = None
    errors = {}
    try:
      data = getattr(ctrler, method)(*args, user=request.user, **kwargs)
    except TypeError as e:
      errors[e.__class__.__name__] = "Method {} should be string not {}".format(method, type(method))
    except Exception as e:
      errors[e.__class__.__name__] = str(e)
    return JsonResponse({"success": not errors, "errors": errors, "data": data})

  def action_join(self, request, game_id):
    errors = {}
    user = request.user
    thread = get_game_thread(game_id)
    thread.main_go.join(user.id, user.username)
    return JsonResponse(dict(success=not errors, errors=errors, data={'joined': True}))

def ensure_thread(game_id):
  thread = get_game_thread(game_id)
  if thread is None:
    logger.info("Thread for Game# {} not loaded, creating".format(game_id))
    game = models.Game.objects.get(pk=game_id)
    thread = GameView._create_thread(game)
  if thread.is_alive() is False:
    logger.info("Hoop Scotch!, Game# {}'s not running!".format(game_id))
    thread.start()
  return thread

class GameView(HandlerView):
  def action_create_game(self, request):
    errors = {}
    
    # create game record
    game = models.Game()
    game.save()

    # launch game thread
    thread = self._create_thread(game)
    thread.start()
    if request.GET.get('play_now') or request.POST.get('play_now'):
      return redirect('game_play', game_id=game.id)
    return JsonResponse(dict(success=not errors, errors=errors, data={'game_id': game.id}))

  @classmethod
  def _create_thread(cls, game):
    global threads
    thread = GameThread(game.id, game.state)
    store_game_thread(game.id, thread)
    return thread

  def action_kill(self, request):
    global threads
    errors = {}
    states = {}
    for k,v in threads.items():
      v.main_go.die()
      v.join()
    threads = {}
    return JsonResponse(dict(success=not errors, errors=errors, data={'states': states}))

  def action_threads(self, request):
    global threads
    errors = {}
    states = {}
    for k,v in threads.items():
      states[k] = {'id': v.ident, 'is_alive': v.is_alive()}
    return JsonResponse(dict(success=not errors, errors=errors, data={'states': states}))

  def action_delete_game(self, request):
    global threads
    game = models.Game.objects.get(request.GET.get('id'))
    game.delete()
    threads[game.id]

  def action_poll_state(self, request):
    errors = {}
    game_id = request.GET.get('game_id')
    thread = ensure_thread(game_id)
    while(not thread.is_looping()):
      time.sleep(0.1)
    state = thread.main_go.state
    return JsonResponse(dict(success=not errors, errors=errors, data={'state': state}))

  def action_play(self, request, game_id):
    thread = ensure_thread(game_id)
    thread = get_game_thread(game_id)
    if thread is None:
      self.action_poll_state(request)
      thread = get_game_thread(game_id)
    user_id = request.user.id

    context = { 'user_id': user_id, 'game_id': game_id }
    return render(request, 'drawbad/vuegame.html', context)

