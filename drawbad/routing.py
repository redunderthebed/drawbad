from django.urls import path
from drawbad.consumers import GameConsumer
#from channels.routing import route

channel_routing = [
    path('ws/game/<int:game_id>/', GameConsumer), 
]