from django.db import models
from django.contrib.auth import models as authmodels
from jsonfield import JSONField

class Game(models.Model):
  players = models.ManyToManyField(authmodels.User)
  over = models.BooleanField(default=False)
  state = JSONField()