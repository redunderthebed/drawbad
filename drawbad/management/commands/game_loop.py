from django.core.management.base import BaseCommand, CommandError
from drawbad import models

class Command(BaseCommand):
  def add_arguments(self, parser):
    parser.add_argument('--run', action="store_true")

  def handle(self, *args, **options):
    pass