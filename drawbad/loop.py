import itertools
import asyncio
import time
import traceback
import drawbad.common as common
import concurrent
from functools import partial
the_loop = None
last_object_id = 0

class GameObject(object):
  def __init__(self, name=None, loop=None):
    global last_object_id
    self._loop = loop or Loop.get_instance()
    self.id = last_object_id + 1
    last_object_id += 1
    self.name = name or "Unnamed {}".format(self.id)
    self._loop.register_object(self)
    self._last_update = None
    self.logger = common.getLogger("GO.{}.#{}".format(self.__class__.__name__, self.name))
    self.callbacks = {}
  
  def set_callback(self, event, cb):
    cbs = self.callbacks.get(event, [])
    if getattr(cb, '__self__', None) is None:
      cb = partial(cb, self)
    self.logger.info("Appending {} to {} event callbacks".format(cb, event))
    cbs.append(cb)
    self.callbacks[event] = cbs

  async def event(self, event, *args, **kwargs):
    built_in = getattr(self, "on_" + event, None)
    if built_in is None:
      cbs = self.callbacks.get(event, [])
    else:
      cbs = list(itertools.chain([built_in], self.callbacks.get(event, [])))
    self.logger.debug1('Event: {} with {} handlers'.format(event, len(cbs)))
    for cb in cbs:
      await cb(*args, **kwargs)

  def trigger(self, event, timeout, *args, **kwargs):    
    built_in = getattr(self, "on_" + event, None)
    if built_in is None:
      cbs = self.callbacks.get(event, [])
    else:
      cbs = list(itertools.chain([built_in], self.callbacks.get(event, [])))
    self.logger.debug1('Event: {} with {} handlers'.format(event, len(cbs)))
    for cb in cbs:
      if timeout is None or timeout <= 0:
        self._loop.enqueue(cb(*args, **kwargs))
      else:
        self._loop.enqueue_later(cb(*args, **kwargs), timeout)

  async def _update(self):
    now = self._loop.get_time()
    since = 0
    if self._last_update is not None:
      since = now - self._last_update
    await self.update(since)
    self._last_update = now

  async def update(self, since):
    pass

  def enqueue(self, co):
    self._loop.enqueue(co)

  async def on_die(self):
    self.logger.info("{} Dying".format(self.name))
    self._loop.forget_object(self)

  def __repr__(self):
    return "GO: {}".format(self.name)

class Loop(object):
  def __init__(self, new_loop=False):
    if new_loop:
      self.loop = asyncio.new_event_loop()
      asyncio.set_event_loop(self.loop)
    else:
      self.loop = asyncio.get_event_loop()
    self.objects = set()
    self.stopped = False
    self.queues = [asyncio.Queue(), asyncio.PriorityQueue(), asyncio.PriorityQueue()]
    self.dummy_time = None
    self.workers = []
    self.future_workers = []
    self.errors = []
    self.logger = common.getLogger('loop')
    # test flags
    self.test = False
    self.loud_clock = False

  @classmethod
  def get_instance(cls):
    global the_loop
    if the_loop is None:
      the_loop = cls()
    return the_loop

  @classmethod
  def destroy_instance(cls):
    global the_loop
    the_loop.objects = set()
    the_loop = None

  def set_test_mode(self, on, loud_clock=False):
    self.test = on
    self.loud_clock = loud_clock
    if self.test:
      self.dummy_time = self.get_time()

  def get_time(self):
    if self.dummy_time:
      return self.dummy_time
    return time.time()

  def instant(self, co):
    return self.loop.run_until_complete(co)

  def bump_time(self, delta):
    self.dummy_time += delta

  def register_object(self, go):
    self.objects.add(go)

  def forget_object(self, go):
    self.objects.remove(go)

  def enqueue(self, co):
    assert asyncio.iscoroutine(co), "{} is {} not coroutine".format(co, type(co))
    self.queues[0].put_nowait(co)

  def enqueue_later(self, co, delay):
    assert asyncio.iscoroutine(co)
    self.queues[1].put_nowait((self.get_time() + delay, str(co), co))

  async def worker(self, worker_id, queue_id):
    try:
      logger = common.getLogger("Worker.{}".format(worker_id))
      while self.stopped is False:
        try:
          queue = self.queues[queue_id]
          f = await queue.get()
          if queue_id == 2:
            now = self.get_time()
            ts, n, f = f
            if ts > now:
              self.debug1("Sleeping {}".format(ts - now))
              print("sleeping {}".format(ts - now))
              await self.sleep(ts - now)
            await f
          else:
            await f
          self.queues[queue_id].task_done()
          #print("task completed by worker {}, {} remaining".format(worker_id, self.queues[0].qsize()))
        except asyncio.QueueEmpty as qe:
          print("queue {} empty".format(queue_id))
        except concurrent.futures._base.CancelledError as ce:
          #print("ce", queue.qsize()) 
          raise
        except Exception as e:
          self.logger.error("Problem occured while processing job {} from queue {}: {}".format(f, queue_id, type(e)))
          self.errors.append(e)
          traceback.print_exc()
          queue.task_done()
          self.stopped = True
          while queue.qsize() > 0:
            f = await queue.get()
            self.queues[queue_id].task_done()
          print("Done")
          await asyncio.sleep(1)

    except asyncio.CancelledError as ce:
      pass
      self.logger.info("Worker {} for queue {} cancelled".format(worker_id, queue_id))

  async def sleep(self, interval):
    if self.test:
      self.bump_time(interval)
    else:
      await asyncio.sleep(interval)

  async def main(self, lifetime=None, interval=1.0):
    started = self.get_time()

    self.workers = []
    for i in range(0, 2):
      self.workers.append(self.loop.create_task(self.worker(i, 0)))
    self.workers.append(self.loop.create_task(self.worker('f1', 2)))
    while self.stopped is False:
      tick = self.get_time()
      if self.loud_clock is True:
        print("TICK! - {}".format(tick-started))

      # Update all the objects
      for go in self.objects:
        self.queues[0].put_nowait(
          go._update()
        )
      # Wait for objects to finish updating
      await self.queues[0].join()

      """ Transfer everything that is ready from the future queue to the
      now queue """
      try:
        job = self.queues[1].get_nowait()
        while job[0] < tick + interval:
          self.queues[2].put_nowait(job)
          job = None
          job = self.queues[1].get_nowait()
        if job is not None:
          self.queues[1].put_nowait(job)
      except asyncio.QueueEmpty as qe:
        pass

      # wait for the scheduled tasks that are ready to complete
      await self.queues[2].join()
      tock = self.get_time()
      took = tock - tick
      if took < interval:
        await self.sleep(interval - took)

      if lifetime is not None and self.get_time() - started > lifetime:
        self.logger.info("Loop reached end of lifetime")
        break
      if len(self.objects) == 0:
        self.logger.info("All the GameObjects in the game are dead")
        break

    if self.stopped is True or self.stopped == "ERROR":
      self.end()
    else:
      self.pause()

  def pause(self):
    for worker in self.workers:
      worker.cancel()

  def end(self):
    for worker in self.workers:
      worker.cancel()
    for i in range(len(self.queues)):
      try:
        while True:
          job = self.queues[i].get_nowait()
          job[1].close()
      except asyncio.QueueEmpty as qe:
        pass

  def start(self, limit=None, interval=1.0):
    self.loop.run_until_complete(self.main(limit, interval))

  def stop(self):
    self.logger.debug("Stopping")
    self.stopped = True

if __name__ == '__main__':
    loop = Loop.get_instance()
    loop.set_test_mode(True)
    go1 = GameObject("obj 1")
    go2 = GameObject("obj 2")
    go1.trigger('die', 1)
    go2.trigger('die', 3.6)
    loop.start(3, 0.5)
