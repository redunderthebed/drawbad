"""
WSGI config for draw project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/wsgi/
"""

import os
import signal

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'draw.settings')

application = get_wsgi_application()

def handler(signal_received, frame):
  print("Received {}".format(signal_received))
 