"""draw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib import auth
from drawbad import views as drawbad_views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('drawbad/', drawbad_views.index),
    path('drawbad/gameview/', drawbad_views.GameView.as_view(), name="gameview"),
    path('drawbad/game/<int:game_id>/action/', drawbad_views.GameActionView.as_view()),
    path('drawbad/game/<int:game_id>/play/', drawbad_views.GameView.as_view(action='play'), name="game_play"),
    path('drawbad/game/create_game/', drawbad_views.GameView.as_view(action="create_game"), name="game_create"),
    # path('drawbad/game/create', drawbad_views.game_create, name="game_create"),
    # path('drawbad/game/<int:game_id>/play', drawbad_views.game_play, name="game_play"),
    # path('drawbad/game/<int:game_id>/lobby', drawbad_views.game_lobby, name="game_lobby"),
    # path('drawbad/game/<int:game_id>/lobby/ready', drawbad_views.game_lobby_ready, name="game_lobby_ready")
]

urlpatterns += [
    path('accounts/', include('django.contrib.auth.urls')),
]